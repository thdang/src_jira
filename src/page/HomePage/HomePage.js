import React from "react";
import Main from "../../component/Main";
import Modal from "../../component/Modal/Modal";
import "../../index.css";

export default function HomePage() {
  return (
    <div>
      <Main />
      <Modal />
    </div>
  );
}
