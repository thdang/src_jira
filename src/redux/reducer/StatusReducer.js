import { GET_ALL_STATUS } from "../contants/contants";

const initialState = {
    arrStatus: [],
};

export const StatusReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_STATUS: {
            state.arrStatus = action.payload;
            return { ...state };
        }

        default:
            return state;
    }
};
