export const ACCESSTOKEN = "ACCESSTOKEN";

export const localStorageService = {
  // luu
  setUserInfor: (user) => {
    let dataJson = JSON.stringify(user);
    localStorage.setItem(ACCESSTOKEN, dataJson);
  },
  // goi ra
  getUserInfor: () => {
    let dataJson = localStorage.getItem(ACCESSTOKEN);
    if (dataJson) {
      return JSON.parse(dataJson);
    } else {
      return null;
    }
  },
};
