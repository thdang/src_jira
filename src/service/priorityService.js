import axios from "axios";

export const priorityService = {
  getPriority: () => {
    return axios({
      method: "GET",
      url: `https://casestudy.cyberlearn.vn/api/Priority/getAll`,
    });
  },
};
