// import { Axios } from "axios";

import axios from "axios";
import { ACCESSTOKEN, localStorageService } from "./localStorageService";

export const userService = {
  postDangNhap: (values) => {
    return axios({
      method: "POST",
      url: `https://casestudy.cyberlearn.vn/api/Users/signin`,
      data: values,
    });
  },

  getUser: (keyWord) => {
    return axios({
      method: "GET",
      url: `https://casestudy.cyberlearn.vn/api/Users/getUser?keyword=${keyWord}`,
      headers: {
        Authorization:
          "Bearer " + localStorageService.getUserInfor(ACCESSTOKEN),
      },
    });
  },
};
