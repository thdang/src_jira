import axios from "axios";

export const statusService = {
    getAllStatus: () => {
        return axios({
            method: "GET",
            url: `https://casestudy.cyberlearn.vn/api/Status/getAll`,
        });
    },
};
